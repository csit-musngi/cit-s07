USE blog_db;

CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(100) NOT NULL,
password VARCHAR(300) NOT NULL,
datetime_created DATETIME NOT NULL,
PRIMARY KEY(id)
);

CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
author_id INT NOT NULL,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_posts_author_id
    FOREIGN KEY (author_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

CREATE TABLE posts_comments(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_commented DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_posts_comments_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_posts_comments_user_id
    FOREIGN KEY (user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

CREATE TABLE posts_likes(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_posts_likes_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_posts_likes_user_id
    FOREIGN KEY (user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1,"First Code", "Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1,"Second Code", "Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (2,"Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (4,"Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

select * from posts where author_id = 1;
+----+-----------+-------------+--------------+---------------------+
| id | author_id | title       | content      | datetime_posted     |
+----+-----------+-------------+--------------+---------------------+
|  1 |         1 | First Code  | Hello World! | 2021-01-02 01:00:00 |
|  2 |         1 | Second Code | Hello Earth! | 2021-01-02 02:00:00 |
+----+-----------+-------------+--------------+---------------------+


select email,datetime_created from users;
+-------------------------+---------------------+
| email                   | datetime_created    |
+-------------------------+---------------------+
| johnsmith@gmail.com     | 2021-01-01 01:00:00 |
| juandelacruz@gmail.com  | 2021-01-01 02:00:00 |
| janesmith@gmail.com     | 2021-01-01 03:00:00 |
| mariadelacruz@gmail.com | 2021-01-01 04:00:00 |
| johndoe@gmail.com       | 2021-01-01 05:00:00 |
+-------------------------+---------------------+


update posts set content = "Hello people of the Earth!" WHERE content = "Hello Earth!";

DELETE FROM users WHERE email = "johndoe@gmail.com";